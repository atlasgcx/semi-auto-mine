export function setup(ctx) {
    const id = 'semi-auto-mine';
    const title = 'SEMI Auto Mine';
    const gem = 'Gem';

    // setting groups
	const SETTINGS_GENERAL = 'General';

    // Sort by experience, not reverse milestone order
    const rockPriority = game.mining.actions.allObjects.sort((a,b) => {
        return b.baseExperience - a.baseExperience
    });


    const adjustSelectedRock = (selectedRock = undefined) => {
        // Are we even mining right now?
        if(game.activeAction !== game.mining) {
            return;
        }

        const autoMineEnabled = ctx.settings.section(SETTINGS_GENERAL).get(`${id}-enable`);
		const autoMineGemsEnabled = ctx.settings.section(SETTINGS_GENERAL).get(`${id}-enable-gems`);

        if (!autoMineEnabled) {
            return;
        }

        // If yes, start looking for an open rock
        rockPriority.every(rock => {
            // Skip this rock if it's a gem and the option is disabled
            if (rock.type.valueOf() == gem.valueOf() && !autoMineGemsEnabled) {
                return true;
            }

            if(game.mining.canMineOre(rock) && !rock.isRespawning && rock.currentHP > 0) {
                // Prevents an edge case where the onRockClick we are catching is the one thrown by this function. Exits either way.
                // Also prevents an error where the game attempts to select our currently selected rock, stopping mining
                if(rock !== selectedRock && game.mining.selectedRock !== rock) {
                    game.mining.onRockClick(rock)
                }
                return false;
            }
            return true;
        })
    }

    // After a rock respawns, check if we want to switch back to it
    ctx.patch(Mining, 'respawnRock').after(() => {
        adjustSelectedRock()
    })

    // We also want to check if our rock runs out of hp
    ctx.patch(Mining, 'startRespawningRock').after(() => {
        adjustSelectedRock()
    })

    // If we select a depleted rock, it should also move over
    ctx.patch(Mining, 'onRockClick').before((rock) => {
        adjustSelectedRock(rock)
    }) 

	// settings
	ctx.settings.section(SETTINGS_GENERAL).add({
		'type': 'switch',
		'name': `${id}-enable`,
		'label': `Enable ${title}`,
		'default': true
	});

    ctx.settings.section(SETTINGS_GENERAL).add({
		'type': 'switch',
		'name': `${id}-enable-gems`,
		'label': `Enable Mining Gem Veins`,
		'default': false
	});

    mod.api.SEMI.log(id, 'Successfully loaded!')
}
